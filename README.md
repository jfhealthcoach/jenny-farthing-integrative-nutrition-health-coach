Welcome. I am so glad you are here.
I am a true lover of life and believe that it is about our journey here on this earth.

I want to make each day count. What this means for me is to be healthy (physically, emotionally and spiritually). It means for me to be focused and motivated to live my life on purpose.

Finding balance in our daily lives can be tricky with the fast pace of our society and all that we are juggling. Do you ever feel like you are living in a hamster wheel, just doing the motions, spinning round and round repetitively?

I am here to support you in finding your healthiest self in this life. We all have the answers and the intuition that we need. It is a matter of getting in tune with learning how to listen to our body and trusting ourselves.

It is the small things that make the dramatic changes!